import { PersonModel } from '../../models/person';

export const resolver = {
    Query: {
        getAllPerson: () => PersonModel.getAllPerson(),
        findPerson(root, { id }) {
            return PersonModel.findPerson(id);
        }
    },
    Mutation: {
        createPerson(root, { input }) {
            return PersonModel.createPerson(input);
        },
        updatePerson(root, { id, input }) {
            if (id) {
                return PersonModel.updatePerson(id, input);
            }
            return false;
        }
    }
}
