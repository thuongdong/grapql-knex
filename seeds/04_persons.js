
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('persons').truncate()
    .then(function () {
      // Inserts seed entries
      return knex('persons').insert([
        {id: 1, name: 'tuong', age: '22', gender: 'male'},
        {id: 2, name: 'hung', age: '5', gender: 'female'},
      ]);
    });
};
